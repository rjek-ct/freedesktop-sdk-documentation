$("[data-tabulated]").wrapInner('<div class="tabs__content p-4"></div>');

$("[data-tabulated]").prepend(
    '<div class="tabs__tabs d-flex flex-wrap"></div> '
);

let tabs = [];

$.each($("[data-tabulated]").find("[data-title]"), function (i, v) {
    if ($(this).index() != 0) {
        $(this)
            .closest("[data-tabulated]")
            .find(".tabs__tabs")
            .append(
                '<div class="tabs__tab py-2 px-3">' +
                    $(this).attr("data-title") +
                    "</div>"
            );
    } else {
        $(this).addClass("active");
        $(this)
            .closest("[data-tabulated]")
            .find(".tabs__tabs")
            .append(
                '<div class="tabs__tab active py-2 px-3">' +
                    $(this).attr("data-title") +
                    "</div>"
            );
    }
}).after(function (i, v) {
    let selector = $(this).index() + 1;
    $(this)
        .closest("[data-tabulated]")
        .find(".tabs__tabs .tabs__tab:nth-child(" + selector + ")")
        .on("click", function () {
            selector = $(this).index() + 1;
            $(this)
                .closest(".tabs__tabs")
                .find(".active")
                .removeClass("active");
            $(this).addClass("active");
            $(this)
                .parent()
                .parent()
                .find(".tabs__content div.active")
                .removeClass("active");
            $(this)
                .parent()
                .parent()
                .find(".tabs__content div:nth-child(" + selector + ")")
                .addClass("active");
        });
});
