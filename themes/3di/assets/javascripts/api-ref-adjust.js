// Use jQuery to update the Redoc output
// Works along with styles in api-ref-styles.css

$(document).ready(function () {
    // Align the left nav font sizes
    $("[role=menuitem]").css("font-size", "16px");
    $("span.operation-type").css({
        "font-size": "12px",
        "font-weight": "regular",
    });
    // Nudge down http verb labels (left nav only)
    $("[role=menuitem] > span.operation-type").css({
        "font-size": "12px",
        "margin-top": "0.35em",
        "font-weight": "regular",
    });
    // Reduce padding on main content sections
    $("div.api-content > div").css({
        padding: "1em 0",
    });
    // Reduce margins on h1 and h2 to reduce whitespace
    $("h1").css({
        margin: "0.5em 0",
    });
    $("h2").css({
        margin: "0.5em 0",
    });
    // Adjust font size and margin for sidebar h3
    $("div.api-content > div > div > div > div > h3").css({
        "font-size": "16px",
        margin: "0.5em 0",
    });
    // Add clickable return to site root on logo
    $("div.menu-content > div > img").wrap("<a href='/'></a>");

    // Change styles for sticky side-menu
    $(".menu-content").css({
        "border-right": "1px solid #4f21851a",
        position: "sticky",
        "overflow-y": "auto",
    });
    // Adjust size of horizontal line
    $("[data-section-id]::after").css({
        // TODO: find solution for better selector
        width: "calc(100% - 250px)",
    });
    // Now we can actually display the Redoc section
    $(".no-js").removeClass("no-js");
});

//Styles for mobile devices are in api-ref-styles.css in ../css folder
