---
title: "Updating the SDK"
linkTitle: "Updating"
infoType: hub
menu: primary
icon: updating
weight: 7
---
Learn how to update Freedesktop SDK or point to a specific version of the SDK.

<!--more-->

{{<tip "Tip">}}
Freedesktop SDK has a major release every year, and each release has a maintenance period of two years. For more information about the releases, see [Release notes]({{< ref "/content/updating-sdk/release-notes.md" >}}).
{{</tip>}}

As a Freedesktop SDK contributor, you can access the latest version of the SDK with `git pull` on the latest release branch.

As a developer using Freedesktop SDK in your own project, you can set the junction element to track a specific version of Freedesktop SDK. For more information, see [Specifying a version to use]({{< ref "/content/updating-sdk/specifying-version-to-use.md" >}}).
