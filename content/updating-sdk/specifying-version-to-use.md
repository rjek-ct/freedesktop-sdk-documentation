---
title: "Specifying a version to use"
weight: 2
infoType: task
---
Learn how to change the version of Freedesktop SDK in your project and update specific project elements.

<!--more-->

## Changing the version in the junction element

If you use a junction to Freedesktop SDK in your project, you can change which version of the SDK your project tracks for updates.

Your BuildStream junction element contains the `source:` dictionary that determines the Freedesktop SDK version to use in your project.

{{<tip "Tip">}}
The `git_repo` plugin enables the wildcard syntax when tracking the source for the junction.
{{</tip>}}

For more information on junction elements for Freedesktop SDK, see [Using the SDK as a junction]({{< ref "/content/using-the-sdk/using-sdk-junction.md" >}}).

**To change the tracked version of Freedesktop SDK in your project, follow these steps:**

1. Open your preferred terminal.
2. In your project directory, find and open the Freedesktop SDK junction element, for example:

    ```yaml
    kind: junction
    sources:
      - kind: git_repo
        url: gitlab:freedesktop-sdk/freedesktop-sdk.git
        track: freedesktop-sdk-22.08*
        ref: freedesktop-sdk-22.08.0-0-7333990bfdede7e8dfbd5efae25c11228b78c0
    ```

3. Change the `track:` value to the version that you want to use, for example:

    ```yaml
    kind: junction
    sources:
      - kind: git_repo
        url: gitlab:freedesktop-sdk/freedesktop-sdk.git
        track: freedesktop-sdk-23.08*
        ref: freedesktop-sdk-23.08.0-0-g1533f8d0393984f5684985c8b710e9fbaf9c8734
    ```

4. Save the `.bst` junction element.
5. [Update the `ref` by tracking the newly specified pattern]({{< ref "/content/updating-sdk/specifying-version-to-use.md#updating-a-specific-element" >}}).

Your BuildStream project now tracks the Git references pattern of `freedesktop-sdk-23.08*`. The `*` wildcard enables BuildStream to keep up with future updates to the `23.08` versions of Freedesktop SDK.

## Updating a specific element

With the `bst source track` command, you can use the latest available state (`ref`) for a specific element.

**To update a specific element, follow these steps:**

1. Open your preferred terminal.
2. In your project directory, identify the element that you want to update.

    For example, you have a `freedesktop-sdk.bst` junction element that uses the stable `freedesktop-sdk-23.08.0` version, as indicated in the `ref:` key:

    ```yaml
    kind: junction
    sources:
      - kind: git_repo
        url: gitlab:freedesktop-sdk/freedesktop-sdk.git
        track: freedesktop-sdk-23.08*
        ref: freedesktop-sdk-23.08.0-0-g1533f8d0393984f5684985c8b710e9fbaf9c8734
    ```

    Now, you want to use the newly released `freedesktop-sdk-23.08.1` patch version for testing.

3. Update the specified element to the latest available version by entering the following command:

    ```console
    bst source track <element-to-update>
    ```

   {{<tip "Tip">}}
   If you're updating an element that has dependencies, you can update the element with all its dependencies by appending the `bst source track` command with `--deps all`.
   {{</tip>}}

4. To see the version changes for the element, enter the following command:

    ```shell
    git diff
    ```

5. Apply the version changes for the element by entering the following command:

    ```console
    git commit -am "My commit message"
    ```

    In the example `freedesktop-sdk.bst` junction element, the `ref:` key now reflects the latest available version:

    ```yaml
    kind: junction
    sources:
      - kind: git_repo
        url: gitlab:freedesktop-sdk/freedesktop-sdk.git
        track: freedesktop-sdk-23.08*
        ref: freedesktop-sdk-23.08.1-0-g82ccd27bb92aa3934f8c2ff62bbe46ae4af7adf4
    ```
