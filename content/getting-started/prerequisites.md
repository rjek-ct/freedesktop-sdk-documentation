---
title: "Prerequisites and dependencies"
weight: 2
infoType: concept
---

You need to install specific tools and dependencies to start using Freedesktop SDK.

<!--more-->

{{<tip "Prerequisites">}}

- Install and configure your local Git environment.
- Install [BuildStream 2](https://buildstream.build/install.html).
    - For more information about using BuildStream 2, see the [BuildStream 2 documentation](https://docs.buildstream.build/2.0/index.html).
- Install the following command line tools:
    - `fusermount3` (part of the `fuse3` package)
    - `lzip`
    - `patch`
- Install the following Python packages:
    - `dulwich`
    - `requests`
    - `packaging`
- Install [GNU Make](https://www.gnu.org/software/make/).
{{</tip>}}
