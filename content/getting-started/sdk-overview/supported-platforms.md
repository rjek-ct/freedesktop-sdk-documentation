---
title: "Supported platforms"
weight: 2
infoType: concept
---
You can use Freedesktop SDK on platforms that support BuildStream.

<!--more-->
Freedesktop SDK uses BuildStream as a build tool. Currently, the platforms that BuildStream natively supports are Linux distributions only. If you want to use the SDK definitions and build outputs, you must install BuildStream with dependencies on a Linux-based system or WSL.

For more information on installing and configuring BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
