---
title: "Initial setup"
weight: 4
infoType: getting-started
---
Learn what to install and configure to begin working with Freedesktop SDK.

<!--more-->
You need BuildStream 2, the Freedesktop SDK repository cloned to your local machine, and Make as the initial setup that allows you to start using Freedesktop SDK.

{{<note "Note">}}
[BuildStream 2](https://docs.buildstream.build/2.0/index.html) allows you to create software stacks using a single metadata format instead of several different formats.

[Make](https://www.gnu.org/software/make/) allows you to run `make` commands that execute BuildStream builds using the pre-configured `Makefile` in the Freedesktop SDK repository.
{{</note>}}

**To start with Freedesktop SDK, follow these steps:**

1. Install [BuildStream 2](https://docs.buildstream.build/2.0/main_install.html).
2. Install [GNU Make](https://www.gnu.org/software/make/).
3. Open a terminal of your choice.
4. Choose how to clone the Freedesktop SDK repository:
   - To clone using HTTPS, enter the following command:

        ```shell
        git clone https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git
        ```

   - To clone using SSH, enter the following command:

        ```shell
        git clone git@gitlab.com:freedesktop-sdk/freedesktop-sdk.git
        ```

5. To run a minimal SDK build, enter the following command:

      ```shell
      make minimal
      ```

{{<note "Note">}}
For more information on other types of builds, see [Building outputs using the SDK]({{< ref "/content/using-the-sdk/building-outputs/_index.md" >}}).
{{</note>}}
