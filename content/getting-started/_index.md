---
title: "Getting started"
infoType: hub
menu: primary
icon: getting-started
weight: 3
---
Learn how to start using Freedesktop SDK.

<!--more-->

This section of the guide focuses on an overview of the Freedesktop SDK project, how to access the project definitions and get a sample project running. You can also learn about specific tools and dependencies that you need to install and configure on your computer, such as BuildStream.

{{<tip "Tip">}}
This guide uses a lot of BuildStream-related terms. For more information about the BuildStream glossary, see the [BuildStream documentation](https://docs.buildstream.build/2.1/main_glossary.html).
{{</tip>}}
