---
title: "Using the SDK"
infoType: hub
menu: primary
icon: task
weight: 3
---
Learn how to use Freedesktop SDK in your software development and integration projects.

<!--more-->

This section of the guide focuses on the most common scenarios in which you can use Freedesktop SDK. These scenarios include building different outputs from the same set of components, as well as some common development tasks, such as using the SDK as a junction and overriding the default SDK components.
