---
title: "Using the SDK as a junction"
weight: 10
infoType: task
---

Use the BuildStream junction element to integrate your project with the Freedesktop SDK components.

<!--more-->

[Junction](https://docs.buildstream.build/master/junctions/junction-elements.html) is a BuildStream feature. With a junction element in your project, you can reference a specific version of Freedesktop SDK to use as a base for your own BuildStream project.

For example, the [`gnome-build-meta`](https://gitlab.gnome.org/GNOME/gnome-build-meta/blob/master/elements/freedesktop-sdk.bst) project defines Freedesktop SDK as a junction to use the Freedesktop SDK components and compilers.

{{<tip "Prerequisites">}}

Make sure you have an existing BuildStream project.

For more information about configuring BuildStream projects, see the [BuildStream documentation](https://docs.buildstream.build/2.0/tutorial/first-project.html).
{{</tip>}}

**To use Freedesktop SDK as a junction, follow these steps:**

1. In your BuildStream elements directory, create a `.bst` junction element.
2. In the junction element, specify Freedesktop SDK as the BuildStream project resource, as in the following example:

    ```yaml
    kind: junction
        sources:
        - kind: git_repo
          url: gitlab:freedesktop-sdk/freedesktop-sdk.git
          track: freedesktop-sdk-23.08*
          ref: freedesktop-sdk-23.08.0-0-g1533f8d0393984f5684985c8b710e9fbaf9c8734
    ```

    In the example, the junction element uses the following keys and values:

    | Key | Description |
    | ------------- | ------------- |
    | `kind:` | Specifies the type of the element. <br> In the example, the `junction` value identifies the `.bst` file as a BuildStream junction element.  |
    | `sources:` | Lists sources from which BuildStream fetches the external project. |
    | `kind:` | Specifies the source type of the external project. <br> In the example, BuildStream uses the `git_repo` plugin from [bst-plugins-experimental](https://gitlab.com/BuildStream/bst-plugins-experimental). |
    | `url:` | Specifies the URL of the Git repository from which BuildStream fetches the resources. <br> In the example, it's the Freedesktop SDK GitLab repository. |
    | `track:` | Specifies the Git references pattern to track from the Git repository. <br> In the example, BuildStream tracks the Git references pattern of `freedesktop-sdk-23.08*`. The `*` wildcard allows BuildStream to keep up with future updates in the external project. |
    | `ref:` | Instructs BuildStream to use a specific state of the external project. <br> In the example, the configuration points to a specific commit identifier: `freedesktop-sdk-23.08.0-0-g1533f8d0393984f5684985c8b710e9fbaf9c8734`. |

3. Save the `.bst` junction element.

For more information about using the BuidStream junction element, see the [BuildStream documentation](https://docs.buildstream.build/2.0/junctions/junction-elements.html).
