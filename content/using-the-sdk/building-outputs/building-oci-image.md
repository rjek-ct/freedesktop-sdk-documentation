---
title: "Building an OCI image using the SDK"
linkTitle: "Building OCI images"
weight: 3
infoType: task
icon: task
---
Learn how to build an OCI image using Freedesktop SDK.

<!--more-->

## Building the Freedesktop SDK OCI images

With Freedesktop SDK, you can build OCI images that are easy to integrate with other tools and environments. Freedesktop SDK and BuildStream help you create these images in a reproducible and consistent manner. You can then run these images in container runtimes or include them in your CI/CD pipeline configuration.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
- If you want to run OCI images, make sure to install a container runtime, such as [Docker](https://www.docker.com/get-started/) or [Podman](https://podman.io/get-started).
{{</tip>}}

**To build OCI images available in Freedesktop SDK, follow these steps:**

1. Open your preferred terminal and go to the root folder of the `freedesktop-sdk` repository.
2. Build the OCI images by entering the following command:

   ```shell
   make export-oci
   ```

By default, the build process creates the following archived OCI images in the root of the `freedesktop-sdk` project:

- `debug-oci.tar`
- `flatpak-oci.tar`
- `platform-oci.tar`
- `sdk-oci.tar`
- `toolbox-oci.tar`

**To run an OCI image, follow these steps:**

1. Open your preferred terminal.
2. Run an OCI image in a container runtime, for example, `platform-oci.tar` in Podman, by entering the following command:

   ```shell
   podman load -i platform-oci.tar
   ```

## Building OCI images in a downstream project

You can use the Freedesktop SDK components to help build container images in your own BuildStream project.

For example, the [`freedesktop-sdk-docker-images`](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images) project uses `freedesktop-sdk` as a junction to help build various project images.

In that project, the [`freedesktop-sdk.bst`](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/blob/ed29a98cf21fed9a6bc9d241cff4c8e3bc825289/elements/freedesktop-sdk.bst) file defines Freedesktop SDK as a junction:

```yaml
kind: junction

config:
  options:
    target_arch: "%{arch}"

sources:
- kind: git_tag
  url: https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git
  track: release/23.08
  ref: freedesktop-sdk-23.08.2-0-geb7333990bfdede7e8dfbd5efae25c11228b78c0
```

The project's [CI build configuration](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/blob/ed29a98cf21fed9a6bc9d241cff4c8e3bc825289/.gitlab-ci.yml#L92) contains BuildStream commands, which are responsible for building specific image elements, for example the `builder-bst2-docker.bst` element:

```yaml
...
.build_and_publish:
  stage: "build"
  needs: []
  script:
    - podman info
    - |
      ${BST} build builder-bst2-docker.bst \
                   builder-bst16-docker.bst \
                   buildbox-casd-docker.bst \
                   lorry-docker.bst \
                   marge-bot-docker.bst
...
```

The [`builder-bst2-docker.bst`](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/blob/ed29a98cf21fed9a6bc9d241cff4c8e3bc825289/elements/builder-bst2-docker.bst#L4) element depends on a specific element from Freedesktop SDK, `freedesktop-sdk.bst:oci/oci-builder.bst`, to help build the image:

```yaml
kind: script

build-depends:
- freedesktop-sdk.bst:oci/oci-builder.bst
- common-layer-init-scripts.bst
- filename: common-layer-oci.bst
  config:
    location: /parent
- filename: builder-bst2-image.bst
  config:
    location: /layer

config:
  commands:
  - |
    if [ -d /initial_scripts ]; then
      for i in /initial_scripts/*; do
        "${i}" /layer
      done
    fi
  - |
    cd "%{install-root}"
    build-oci <<EOF
    mode: docker
    images:
    - os: linux
      architecture: "%{go-arch}"
      parent:
        image: /parent
      layer: /layer
      comment: "Builder image for Freedesktop SDK with BuildStream 2"
      tags:
      - "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:latest"
      config:
        Cmd: ["/bin/bash"]
    EOF
```

In this example, the `freedesktop-sdk-docker-images` project uses the `freedesktop-sdk.bst:oci/oci-builder.bst` element to enable the `build-oci` script in the `commands:` dictionary. The [`common-layer-oci.bst`](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/blob/master/elements/common-layer-oci.bst) is the parent image, which makes sure that all images in the `freedesktop-sdk-docker-images` project efficiently share common components.

{{<tip "Tip">}}
If you want to replicate a similar configuration in your project, use the code as in the provided example but customise the following:

- In the `build-depends:` dictionary, replace `filename: common-layer-oci.bst` with your parent image (equivalent of the `FROM` instruction in `Dockerfile`).
    - If you do not have one, you can use the `freedesktop-sdk.bst:oci/bootstrap-oci.bst` element from Freedesktop SDK.
- In the `build-depends:` dictionary, replace `filename: builder-bst2-image.bst` with the element that contains the contents of your container.
- In the `commands:` dictionary, replace `comment:` and `tags:` with content appropriate for your project.
{{</tip>}}
