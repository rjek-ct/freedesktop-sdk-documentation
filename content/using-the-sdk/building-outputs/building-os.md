---
title: "Building OS images using the SDK"
linkTitle: "Building OS images"
weight: 1
infoType: task
icon: task
---
With Freedesktop SDK, you can create an OS image, which you can install in hardware or in a virtual machine for testing or development purposes.

<!--more-->

Freedesktop SDK gives you several templates to start building an operating system. Those templates are starting points for creating a sample OS image rather than complete solutions to build a fully fledged operating system.

As a developer, you can use the OS demonstration versions and components that Freedesktop SDK provides, but the complete development of your OS depends on you.

You can find all the Freedesktop SDK-based [OS demonstration versions](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/tree/master/elements/vm) on GitLab.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
- Install and configure [QEMU](https://www.qemu.org/).
{{</tip>}}

{{<note "Note">}}
The Freedesktop SDK demonstration versions of sample OSs use UEFI to handle the booting process.
All demonstration versions use systemd-boot as bootloader, except the minimal OS build.
{{</note>}}

### Testing

We recommend using [GNOME Boxes](https://flathub.org/apps/org.gnome.Boxes) for testing OS images on a virtual machine. Alternatively, you can use [QEMU](https://www.qemu.org/), a machine emulator and a low-level virtualiser, for testing purposes.

### Building a minimal OS image

The minimal OS build allows you to create a sample OS image that doesn't have a bootloader.

**To build a minimal OS image without a bootloader, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build and run the image by entering the following command:

    ```shell
    make run-vm
    ```

    {{<note "Note">}}
To access the OS, use the default credentials:

- Login: `root`
- Password: `root`
    {{</note>}}

### Building a minimal EFI OS image

The minimal EFI OS build lets you run a fast and lightweight operating system, which you can use for experimental or testing purposes.

**To build a minimal EFI OS image, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build and run the image by entering the following command:

    ```shell
    make run-efi-vm
    ```

    {{<note "Note">}}
To access the OS, use the default credentials:

- Login: `root`
- Password: `root`
    {{</note>}}

### Building a minimal OSTree image

You can build a minimal OSTree image based on the versioning update system
[OSTree](https://ostreedev.github.io/ostree/).

{{<tip "Prerequisites">}}
Install [GNUPG](https://gnupg.org/index.html).
{{</tip>}}

**To build a minimal OSTree OS image, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build and run the image by entering the following command:

    ```shell
    make run-ostree-vm
    ```

3. To build and check out the OSTree repository, and then launch a build server, enter the following command:

    ```shell
    make ostree-serve
    ```

4. To download and deploy the latest OSTree ref from the launched build server, enter the following command:

    ```shell
    ostree admin upgrade
    ```

    {{<note "Note">}}
Reboot the OS for changes to take effect.
        {{</note>}}

5. To rebuild the changes and commit the results to the OSTree repository, enter the following command:

    ```shell
    make update-ostree
    ```

    {{<note "Note">}}
Use this command after you update `freedesktop-sdk` or make local changes.

To access the OS, use the default credentials:

- Login: `root`
- Password: `root`
        {{</note>}}

### Building a secure-boot OS image

You can create a sample OS image with secure boot that uses [OpenSSL](https://www.openssl.org/) keys to encrypt the image so nobody can use it on a different machine.

{{<tip "Prerequisites">}}

If you want to emulate the secure boot in a VM, install [swtpm](https://github.com/stefanberger/swtpm) and [OpenSSL](https://www.openssl.org/).
{{</tip>}}

**To build a secure-boot OS image, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build and run the image by entering the following command:

    ```shell
    make run-secure-vm 
    ```

3. To launch a build server for image updates, enter the following command:

    ```shell
    make secure-images-serve
    ```

4. To download and deploy the latest image from the launched build server, enter the following command:

    ```shell
    /usr/lib/systemd/systemd-sysupdate update
    ```

    {{<note "Note">}}
Reboot the OS for changes to take effect.
    {{</note>}}

5. To build a new image and upload the image to the server, enter the following command:

    ```shell
    make export-secure-images
    ```

{{<note "Note">}}
When the secure OS boots, you can see all the prompts related to a standard first-boot experience, such as setting the date and time or choosing the username and password.
{{</note>}}

### Building a desktop OS image

The desktop build allows you to create a sample OS with a GUI. This build type does not use the `Makefile` because the `Makefile` commands run a VM in text mode, and not as an image with a GUI.

{{<tip "Tip">}}
Remember that building an OS with a GUI can take more time than creating minimal OS builds.
{{</tip>}}

**To build a desktop OS image without the `Makefile`, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build the image by entering the following command:

   ```shell
   bst build vm/desktop/efi.bst
   ```

3. Extract the image to a chosen directory by entering the following command:

    ```shell
    bst artifact checkout vm/desktop/efi.bst --directory vm-image
    ```

4. To make the image bigger, enter the following command:

    ```shell
   truncate --size=+2G vm-image/disk.img
    ```

5. Run the image in your preferred VM software, such as GNOME Boxes.
