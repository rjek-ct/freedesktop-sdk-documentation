---
title: "Contribution best practices"
weight: 3
infoType: concept
icon: concept
---
Learn about best practices for making changes in the Freedesktop SDK project and modifying a Git source.

<!--more-->

## Best practices for commit messages

After making changes related to adding a new feature, modifying an existing one, or fixing a bug, it’s time to commit those changes.

The following recommendations can help you write useful commit messages that improve the contribution workflow:

- Write commit messages that are easy to read.
- Create short subject lines in commits.
- Use sentence-case capitalisation in the subject line.
- Don't use full stops in the subject line.
- Use the imperative in the subject line.
- Use the body to explain what the change introduces and why the change is necessary.
- Don't use more than 72 characters in a single line of a commit message.

## Modifying a Git source

The Freedesktop SDK project uses various tools to track versions and security vulnerabilities.

The Freedesktop SDK project uses the following elements to identify commits:

- The latest tag.
- The number of commits since the latest tag.
- The SHA.

You need to update the "ref" (SHA) in the .bst file for a Git source to a new version. You can use the `bst source track components/element.bst` command, which automatically tracks and updates the reference, eliminating the need for manual changes.

**To manually create a new reference format for the new version, follow these steps:**

In the location where you cloned the relevant repository, enter the following command:

```shell
git describe --tags --long --abbrev=40
```

The terminal displays the following sample response:

```console
glibc-2.38-6-g7ac405a74c6069b0627dc2d8449a82a621f8ff06
```

This format of the response is user-friendly and facilitates automation and parsing of the version by other tools.

{{<note "Note">}}
The Freedesktop SDK CI automatically tracks the tags added to the Git repositories. However, the CI method requires you to use the `git_repo` plugin instead of `git` to add a new Git source.
{{</note>}}
