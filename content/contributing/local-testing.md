---
title: "Local testing"
weight: 2
infoType: concept
icon: concept
---
You can test your local changes to the Freedesktop SDK project by running Make tool commands.

<!--more-->

In the root of the `freedesktop-sdk` repository, there is a `Makefile` that provides several targets for building and testing the SDK.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
{{</tip>}}

## Testing the SDK locally using the Makefile

When your local changes are ready, you can start testing them on your machine using the provided `Makefile`.

**To start testing, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` project.

2. Test your changes by using the available Make commands:

    | Command | Description        |
    | ------------- | ------------- |
    | `make build` | Builds the project.  |
    | `make build-tar` | Builds Freedesktop SDK tarballs. |
    | `make build-vm` | Builds Freedesktop SDK VM images. |
    | `make bootstrap` | Builds and checks out the bootstrap. |
    | `make export` | Builds and checks out as a Flatpak repository. |
    | `make export-snap` | Builds and exports as a snap image. |
    | `make export-tar` | Builds and exports Freedesktop SDK tarballs. |
    | `make export-oci` | Builds and exports Freedesktop SDK OCI images. |
    | `make export-docker` | Builds and exports Freedesktop SDK docker images. |
    | `make run-vm` | Builds and runs Freedesktop SDK VM images. |
    | `make manifest` | Builds and exports manifests for the platform and SDK. |
    | `make markdown-manifest` | Converts manifests to a human-readable format. |
    | `make check-dev-files` | Checks for `dev` files in the Platform. |
    | `make check-rpath` | Checks for components using RPATH. |
    | `make test-apps` | Tests basic apps. <br>Remember to run `make export` before you run `make test-apps`. |
    | `make test-codecs` | Tests if the codec extensions work as intended. |
    | `make track-mesa-git` | Tracks the `mesa-git` extension. |

## Additional variables

You can use additional variables to customise the build:

| Variable | Description           | Default value |
| ------------- | ------------- | ------------- |
| `ARCH` | Exports as a Flatpak with architecture (ARCH).  | `system arch`  |
| `BRANCH` | Gets the name of the Flatpak branch. | `git branch`   |
| `REPO` | Gets the name of local Flatpak repository to export to. | `repo/`   |
| `CHECKOUT_ROOT` | Gets the location to check out runtimes from `bst` checkout. | `runtimes/`   |
