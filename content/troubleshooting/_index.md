---
title: "Troubleshooting"
infoType: hub
menu: primary
icon: troubleshooting
weight: 6
---

Learn about the most common issues that you can encounter when using Freedesktop SDK.

<!--more-->

Freedesktop SDK relies on BuildStream to build and integrate components. As a result, BuildStream-related errors are the reason for most of the issues that can occur in Freedesktop SDK projects.

This section of the guide focuses on the most common troubleshooting scenarios. If you encounter a problem that you cannot solve, contact the Freedesktop SDK developers in one of the following ways:

- [Matrix communication platform](https://matrix.to/#/#freedesktop-sdk:matrix.org)
- [Mailing list](https://lists.freedesktop.org/mailman/listinfo/freedesktop-sdk)

For bugs, raise a GitLab issue in the [`freedesktop-sdk` repository](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues).
