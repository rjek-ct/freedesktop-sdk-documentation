---
title: "Build fails (fuse)"
weight: 3
infoType: troubleshooting
icon: troubleshooting
---
<!--more-->
## Symptom

The Freedesktop SDK build or patch update process displays the following error:

```console
The FUSE stager child process unexpectedly died with exit code 2
```

## Cause

Your configuration is missing the `fusermount3` tool, which is a BuildStream dependency required for `buildbox-fuse`.

## Remedy

Install the missing tool:

```shell
sudo apt install fuse3
```
