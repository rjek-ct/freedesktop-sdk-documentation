---
title: "Build fails (Python)"
weight: 1
infoType: troubleshooting
icon: troubleshooting
---
<!--more-->
## Symptom

The Freedesktop SDK build process displays the following error:

```console
Failed to load source plugin '<plugin>' : No module named '<plugin-dependency>'
```

## Cause

Your configuration is missing the `<plugin-dependency>` Python plugin dependency, which BuildStream requires for a specific `<plugin>` plugin to work.

## Remedy

Install the missing Python dependency:

```shell
pip install <plugin-dependency>
```

## Examples

The following Python plugin dependencies are often missing because of incorrect BuildStream configuration.

### `dulwich`

- **Symptom**:

   ```console
   Failed to load source plugin 'git_repo' : No module named 'dulwich'
   ```

- **Cause**:

   Your configuration is missing the `dulwich` Python plugin dependency, which BuildStream requires for the `git_repo` plugin to work.

- **Remedy**:

   ```shell
   pip install dulwich
   ```

### `requests`

- **Symptom**:

   ```console
   Failed to load source plugin 'docker' : No module named 'requests'
   ```

- **Cause**:

   Your configuration is missing the `requests` Python plugin dependency, which BuildStream requires for the `docker` plugin to work.

- **Remedy**:

   ```shell
   pip install requests
   ```

### `packaging`

- **Symptom**:

   ```console
   Failed to load source plugin 'pypi' : No module named 'packaging'
   ```

- **Cause**:

   Your configuration is missing the `packaging` Python plugin dependency, which BuildStream requires for the `pypi` plugin to work.

- **Remedy**:

   ```shell
   pip install packaging
   ```
